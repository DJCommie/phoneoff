package efedeniz.com.phoneoff;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

/**
 * Created by akliotzk on 6/18/2015.
 */
public class NotificationService extends IntentService {

    final private static String TAG = NotificationService.class.getSimpleName();

    public NotificationService() {
        super("PhoneOffNotificationService");
    }

    @Override
    protected void onHandleIntent(Intent i) {

        Log.d(TAG, "onHandleIntent");
        NotificationController.showNotification(getApplicationContext());
    }
}
