package efedeniz.com.phoneoff;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;

/**
 * Created by akliotzk on 8/10/2015.
 */
public class PopUpNotificationActivity extends Activity {

    private static final String TAG = PopUpNotificationActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_pop_up_notification);

        this.setFinishOnTouchOutside(false);

        // set up buttons
        ImageButton motivationButton = (ImageButton)findViewById(R.id.motivate_button);
        motivationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Motivation button clicked");
                Intent intent = new Intent(PopUpNotificationActivity.this, MotivationActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                PopUpNotificationActivity.this.startActivity(intent);
                finish();
            }
        });

        /*
        ImageButton okButton = (ImageButton)findViewById(R.id.ok_button);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        */

        ImageButton exitButton = (ImageButton)findViewById(R.id.exit_pop_up_button);
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ImageButton settingsButton = (ImageButton)findViewById(R.id.settings_pop_up_button);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PopUpNotificationActivity.this, PhoneOffActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                PopUpNotificationActivity.this.startActivity(intent);
                finish();
            }
        });
    }
}
