package efedeniz.com.phoneoff;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by akliotzk on 6/24/2015.
 */
public class Logger {

    private static final String TAG = Logger.class.getSimpleName();
    private static final String LOG_NAME = "time.log";
    private static final String SCREEN_ON = "ON";
    private static final String SCREEN_OFF = "OFF";

    public static class ScreenOnOffTimes {

        public ArrayList<Long> onTimes = new ArrayList<Long>();
        public ArrayList<Long> offTimes = new ArrayList<Long>();
    }

    public static void log(Context context, boolean isOn)
    {
        // write to a temp file the log so far
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(LOG_NAME, Context.MODE_APPEND));
            Calendar c = Calendar.getInstance();
            String onOrOff = SCREEN_OFF;
            if (isOn) {
                onOrOff = SCREEN_ON;
            }

            Log.d(TAG, "Write line: " + onOrOff + ":" + String.valueOf(c.getTimeInMillis()));
            outputStreamWriter.write(onOrOff + ":" + String.valueOf(c.getTimeInMillis()));
            // write newline for readability
            outputStreamWriter.write("\n");
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e(TAG, "File write failed: " + e.toString());
        }
    }

    public static ScreenOnOffTimes readLog(Context context) {

        ScreenOnOffTimes screenTimes = new ScreenOnOffTimes();

        try {
            InputStream inputStream = context.openFileInput(LOG_NAME);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    if (receiveString.contains(SCREEN_ON)) {
                        Log.d(TAG, "Read line: " + receiveString);
                        Long time = Long.valueOf(receiveString.split(":")[1]);
                        screenTimes.onTimes.add(time);
                    }
                    else if (receiveString.contains(SCREEN_OFF)) {
                        Log.d(TAG, "Read line: " + receiveString);
                        Long time = Long.valueOf(receiveString.split(":")[1]);
                        screenTimes.offTimes.add(time);
                    }
                    else {
                        throw (new IOException("File written incorrectly"));
                    }
                }

                inputStream.close();
            }

        }
        catch (FileNotFoundException e) {
            Log.e(TAG, "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e(TAG, "Can not read file: " + e.toString());
        }

        return screenTimes;
    }

    // clear the log
    public static void clearLog(Context context)
    {
        try {
            new OutputStreamWriter(context.openFileOutput(LOG_NAME, Context.MODE_PRIVATE)).close();
            //new FileOutputStream(TEMP_LOG_NAME).close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
