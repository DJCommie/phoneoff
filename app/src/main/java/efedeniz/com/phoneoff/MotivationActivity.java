package efedeniz.com.phoneoff;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.preference.DialogPreference;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import efedeniz.com.util.IabHelper;
import efedeniz.com.util.IabResult;
import efedeniz.com.util.Inventory;
import efedeniz.com.util.Purchase;

/**
 * Created by akliotzk on 6/19/2015.
 */
public class MotivationActivity extends Activity {

    private IabHelper mHelper;

    private static final String TAG = MotivationActivity.class.getSimpleName();
    private static final String HIDE_ADS_ID = "hide_ads";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_motivation);

        // load the karma image
        ImageButton karmaButton = (ImageButton)findViewById(R.id.karma_button);
        //Bitmap bitmap = decodeSampledBitmapFromResource(getResources(), R.drawable.ying_yang, karmaButton.getWidth(), karmaButton.getHeight());
        karmaButton.setBackground(getResources().getDrawable(R.drawable.ying_yang));

        // show ad
        if (!MySharedPreferences.getIsPaid(getApplicationContext())) {
            Log.d(TAG, "Show ad");
            AdView mAdView = (AdView) findViewById(R.id.adViewMotivation);
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
        }


        // let's load google play services
        mHelper = new IabHelper(this, getResources().getString(R.string.base64_public_key));

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    Log.d(TAG, "Problem setting up In-app Billing: " + result);
                }
                else {
                    // Hooray, IAB is fully set up!
                    Log.d(TAG, "IAB is set up... let's check the in-app purchases");
                    List additionalSkuList = new ArrayList<String>();
                    additionalSkuList.add(HIDE_ADS_ID);
                    mHelper.queryInventoryAsync(true, additionalSkuList, mQueryFinishedListener);
                }
            }
        });

        loadRandomPicture();
        loadRandomQuote();

        // dismiss the notification
        NotificationController.dismissNotification(getApplicationContext());

        ImageButton exitButton = (ImageButton)findViewById(R.id.exit_button);
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // kill activity
                finishAffinity();
                Log.d(TAG, "close activity");
            }
        });

        ImageButton settingsButton = (ImageButton)findViewById(R.id.settings_button);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MotivationActivity.this, PhoneOffActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                MotivationActivity.this.startActivity(intent);
                finish();
            }
        });

        //ImageButton karmaButton = (ImageButton)findViewById(R.id.karma_button);
        karmaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MySharedPreferences.getIsPaid(getApplicationContext())) {
                    // show thank you dialog
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(MotivationActivity.this);
                    alertBuilder.setMessage("By purchasing the hide ads upgrade, you have already earned your karma. Thank you!!");
                    alertBuilder.setPositiveButton("I'm Awesome",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alertDialog = alertBuilder.create();
                    alertDialog.show();
                }
                else {
                    // launch upgrade flow
                    mHelper.launchPurchaseFlow(MotivationActivity.this, HIDE_ADS_ID, 10001,
                            mPurchaseFinishedListener, "");
                    Log.d(TAG, "Launch purchase flow");

                }
            }
        });

        if (MySharedPreferences.getRatePreference(getApplicationContext()) == 0) {
            // let's see if it's time to notify the user
            int numViews = MySharedPreferences.getNumViews(getApplicationContext());

            if (numViews == 5 || numViews == 10 || numViews == 20 || numViews == 50) {
                // show the rate my app dialog
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(MotivationActivity.this);
                alertBuilder.setMessage("Seems like you've been using PhoneOff quite a bit. If you feel that PhoneOff has had a positive influence on you, " +
                        "please consider rating.");
                alertBuilder.setPositiveButton("Rate",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                MySharedPreferences.putRatePreference(getApplicationContext(), 1);

                                final Uri uri = Uri.parse("market://details?id=" + getApplicationContext().getPackageName());
                                final Intent rateAppIntent = new Intent(Intent.ACTION_VIEW, uri);

                                if (getPackageManager().queryIntentActivities(rateAppIntent, 0).size() > 0)
                                {
                                    startActivity(rateAppIntent);
                                }
                                else
                                {
                                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(MotivationActivity.this);
                                    alertBuilder.setMessage("Your device cannot reach the Android Market. Please ensure that Google Play app is installed.");
                                    alertBuilder.setPositiveButton("Got it",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });

                                    AlertDialog alertDialog = alertBuilder.create();
                                    alertDialog.show();
                                }
                                dialog.cancel();
                            }
                        });

                alertBuilder.setNeutralButton("Later", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.cancel();
                    }
                });

                alertBuilder.setNegativeButton("Never", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        MySharedPreferences.putRatePreference(getApplicationContext(), 1);
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog = alertBuilder.create();
                alertDialog.show();
            }
        }

        // increment num days by 1
        MySharedPreferences.putNumViews(getApplicationContext(),
                MySharedPreferences.getNumViews(getApplicationContext()) + 1);

    }



    private void loadRandomPicture() {

        ImageView pictureView = (ImageView)findViewById(R.id.motivation_pic);

        // choose a random pic from list
        final TypedArray imgs = getResources().obtainTypedArray(R.array.pic_list);
        final Random rand = new Random();
        final int rndInt = rand.nextInt(imgs.length());
        final int resID = imgs.getResourceId(rndInt, 0);
        Bitmap bitmap = decodeSampledBitmapFromResource(getResources(), resID, 300, 300);

        pictureView.setImageBitmap(bitmap);
    }

    private void loadRandomQuote() {

        TextView textView = (TextView)findViewById(R.id.motivation_text);

        // choose a random pic from list
        final TypedArray quotes = getResources().obtainTypedArray(R.array.quotes_list);
        final Random rand = new Random();
        final int rndInt = rand.nextInt(quotes.length());
        final String quote = quotes.getString(rndInt);

        textView.setText(quote);
    }

    // helper methods with fitting bitmap
    private static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }

    private static Bitmap decodeSampledBitmapFromResource(Resources res,
                                                         int resId, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }


    IabHelper.QueryInventoryFinishedListener
            mQueryFinishedListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory)
        {
            if (result.isFailure()) {
                Log.d(TAG, "Error from querying IAB");
                return;
            }

            if (inventory != null && inventory.hasDetails(HIDE_ADS_ID)) {

                String hideAdPrice =
                        inventory.getSkuDetails(HIDE_ADS_ID).getPrice();

                Log.d(TAG, "hideAdPrice is " + hideAdPrice);

                if (inventory.hasPurchase(HIDE_ADS_ID)) {
                    Log.d(TAG, "hide ads upgrade purchased");
                }
            }
            else {
                Log.d(TAG, "No information available for HIDE_ADS_ID");
            }

            // update the UI
        }
    };

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
            = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase)
        {
            if (result.isFailure()) {
                Log.d(TAG, "Error purchasing: " + result);
                return;
            }
            else if (purchase.getSku().equals(HIDE_ADS_ID)) {
                Log.d(TAG, "Hide ads purchased!");
                MySharedPreferences.putIsPaid(getApplicationContext(), true);

                Log.d(TAG, "purchased this item - " + purchase.getSku());

                AdView mAdView = (AdView) findViewById(R.id.adViewMotivation);
                mAdView.setVisibility(View.INVISIBLE);

                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(MotivationActivity.this);
                alertBuilder.setMessage("Thank you for purchasing the Hide Ads upgrade. You have chosen to spread the positivity and pay it forward. Karma is coming your way!");
                alertBuilder.setPositiveButton("I'm Awesome",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alertDialog = alertBuilder.create();
                alertDialog.show();
            }
        }
    };

    // Need this for IAB for some reason...
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        }
        else {
            Log.d(TAG, "onActivityResult handled by IABUtil.");
        }
    }
}
