package efedeniz.com.phoneoff;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;
import android.widget.RemoteViews;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

/**
 * Created by akliotzk on 6/18/2015.
 */
public class NotificationController extends BroadcastReceiver {

    private final static String TAG = NotificationController.class.getSimpleName();

    private final static int NOTIFICATION_ID = 999;

    // this means user has launched the PhoneOff UI
    public static void appLaunched(Context context) {

        Log.d(TAG, "appLaunched");
    }

    public static void onScreenOn(Context context) {
        Log.d(TAG, "ACTION_SCREEN_ON");
        scheduleNotification(context);
        logScreenEvent(context, true);
    }

    public static void onScreenOff(Context context) {

        Log.d(TAG, "ACTION_SCREEN_OFF");
        cancelNotification(context);
        dismissNotification(context);
        logScreenEvent(context, false);
    }

    public static void scheduleNotification(Context context) {
        Log.d(TAG, "scheduleNotification");

        if (!MySharedPreferences.getAppEnabled(context)){
            return;
        }
        Intent i = new Intent(context, NotificationService.class);
        PendingIntent pi = PendingIntent.getService(context, NOTIFICATION_ID, i, 0 );

        AlarmManager am = (AlarmManager)(context.getSystemService( Context.ALARM_SERVICE ));
        long alarmTime = getNextNotificationTime(context);
        am.set(AlarmManager.RTC_WAKEUP, alarmTime, pi);
    }

    private static long getNextNotificationTime(Context context) {

        Logger.ScreenOnOffTimes onOffTimes = Logger.readLog(context);
        long screenDurationMillis= TimeUnit.MINUTES.toMillis(MySharedPreferences.getNotificationMinutes(context));
        long rangeMillis = TimeUnit.MINUTES.toMillis(MySharedPreferences.getNotificationRange(context));

        if (screenDurationMillis == rangeMillis) {
            Log.d(TAG, "Set timer " + TimeUnit.MILLISECONDS.toMinutes(screenDurationMillis) + " minutes from now");
            return System.currentTimeMillis() + screenDurationMillis;
        }

        // increments 10 s
        long increment = TimeUnit.SECONDS.toMillis(10);
        long timeAgo = rangeMillis - increment;

        while (getScreenOnDuration(onOffTimes, timeAgo) + increment  < screenDurationMillis) {
            increment += TimeUnit.SECONDS.toMillis(10);
            timeAgo = rangeMillis - increment;
        }
        Log.d(TAG, "Set timer " + TimeUnit.MILLISECONDS.toSeconds(increment) + " seconds from now");

        return System.currentTimeMillis() + increment;
    }

    private static long getScreenOnDuration(Logger.ScreenOnOffTimes onOffTimes, long timeAgo) {

        Calendar currentTime = Calendar.getInstance();

        long screenOnDuration = 0;

        // let's find the first time screen was turned ON after timeAgo
        int earliestOnIndex = -1;
        for (int i = 0; i < onOffTimes.onTimes.size(); i++) {
            Long time = onOffTimes.onTimes.get(i);

            if ((currentTime.getTimeInMillis() - time) <= timeAgo ) {
                earliestOnIndex = i;
                break;
            }
        }

        // let's find the last off time
        int earliestOffIndex = -1;
        for (int i = 0; i < onOffTimes.offTimes.size(); i++) {
            Long time = onOffTimes.offTimes.get(i);

            if ((currentTime.getTimeInMillis() - time) <= timeAgo ) {
                earliestOffIndex = i;
                break;
            }
        }

        // means that screen was on before and after timeAgo range... need to count that
        if (earliestOffIndex >= 0 && earliestOnIndex >= 0) {
            if (onOffTimes.onTimes.get(earliestOnIndex) > onOffTimes.offTimes.get(earliestOffIndex)) {
                screenOnDuration += onOffTimes.offTimes.get(earliestOffIndex) - (currentTime.getTimeInMillis() - timeAgo);
            }


            // need to count other on durations
            for (int i = earliestOnIndex; i < onOffTimes.onTimes.size(); i++) {
                if (onOffTimes.offTimes.size() > i) {
                    screenOnDuration += onOffTimes.offTimes.get(i) - onOffTimes.onTimes.get(i);
                }
            }
        }

        return screenOnDuration;
    }

    public static void cancelNotification(Context context) {
        Log.d(TAG, "cancelNotification");

        AlarmManager am = (AlarmManager)(context.getSystemService( Context.ALARM_SERVICE ));
        Intent i = new Intent(context, NotificationService.class);
        PendingIntent pi = PendingIntent.getService(context, NOTIFICATION_ID, i, 0 );

        am.cancel(pi);
    }


    public static void showNotification(Context context) {

        Log.d(TAG, "ShowNotification");

        Logger.clearLog(context);

        // let's have some sanity checks here...
        if (!MySharedPreferences.getAppEnabled(context)) {
            return;
        }

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        if (!pm.isScreenOn()) {
            return;
        }

        if (MySharedPreferences.getReminderPreference(context) == 1) {

            Intent intent = new Intent(context, MotivationActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pIntent = PendingIntent.getActivity(context, NOTIFICATION_ID, intent, 0);
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            Log.d(TAG, "Build version " + Build.VERSION.SDK_INT);
            Log.d(TAG, "Build release " + Build.VERSION.RELEASE);
            if (Build.VERSION.SDK_INT < 21) {

                Notification noti = new Notification.Builder(context)
                        .setContentTitle("Time to stop using your phone")
                        .setContentText("Tap here for some extra motivation").setSmallIcon(R.drawable.phoneoff_icon_notification)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setContentIntent(pIntent).build();

                // hide the notification after its selected
                //noti.flags |= Notification.FLAG_AUTO_CANCEL;

                notificationManager.notify(NOTIFICATION_ID, noti);
            } else {
                Log.d(TAG, "Fancy intent");

                Intent settingsIntent = new Intent(context, PhoneOffActivity.class);
                settingsIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                PendingIntent pendingIntent = PendingIntent.getActivity(context, 1, settingsIntent, 0);

                RemoteViews notificationView = new RemoteViews(context.getPackageName(), R.layout.notification_layout);
                notificationView.setOnClickPendingIntent(R.id.settings_noti_button, pendingIntent);
                //notificationView.setOnClickFillInIntent(R.id.settings_noti_button, settingsIntent);

                Notification noti = new Notification.Builder(context)
                        .setContentTitle("Time to stop using your phone")
                        .setContentText("Tap here for some extra motivation").setSmallIcon(R.drawable.phoneoff_icon_notification)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setContentIntent(pIntent).build();


                noti.contentView = notificationView;
            /*
            noti.contentIntent = pIntent;
            noti.icon = R.drawable.alert_icon;
            */

                notificationManager.notify(NOTIFICATION_ID, noti);
            }
        }

        else if ( MySharedPreferences.getReminderPreference(context) == 0 ) {
            Intent popUpIntent = new Intent(context, PopUpNotificationActivity.class);
            popUpIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(popUpIntent);
        }

        // should we schedule another notification? YES!
        scheduleNotification(context);
    }

    public static void dismissNotification(Context context) {
        Log.d(TAG, "dismissNotification");
        NotificationManager nMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nMgr.cancel(NOTIFICATION_ID);
    }


    private static void logScreenEvent(Context context, boolean isOn) {

        Logger.ScreenOnOffTimes screenTimes = Logger.readLog(context);
        boolean isLastEventOn = false;
        boolean fileEmpty = true;

        // get the last two items of on and off if file is not empty
        long lastOnEvent = 0, lastOffEvent = 0;
        if (screenTimes.onTimes.size() > 0) {
            lastOnEvent = screenTimes.onTimes.get(screenTimes.onTimes.size() - 1);
            fileEmpty = false;
        }
        if (screenTimes.offTimes.size() > 0) {
            lastOffEvent = screenTimes.offTimes.get(screenTimes.offTimes.size() - 1);
            fileEmpty = false;
        }

        if (lastOnEvent > lastOffEvent) {
            isLastEventOn = true;
        }

        // before logging, check the integrity of the file... if it is not good, then delete and start fresh
        if ( isOn == isLastEventOn && !fileEmpty) {
            Log.d(TAG, "isON - " + isOn + " isLastEventOn - " + isLastEventOn + " fileEmpty - " + fileEmpty);
            Log.d(TAG, "This log is corrupt. Delete and start fresh");
            Logger.clearLog(context);
            fileEmpty = true;
        }

        // do not write OFF_SCREEN if file is empty... it's useless
        if ((fileEmpty && isOn) || !fileEmpty) {
            Logger.log(context, isOn);
        }
    }

    // this will be for boot notification only... do we even need this?
    @Override
    public void onReceive(Context context, Intent intent) {

        scheduleNotification(context.getApplicationContext());
        // let's clear our time recording log just in case
        Logger.clearLog(context.getApplicationContext());

        Log.d(TAG, "ACTION_BOOT_COMPLETED or QUICK_BOOT");
    }

}
