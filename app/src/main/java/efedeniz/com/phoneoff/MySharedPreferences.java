package efedeniz.com.phoneoff;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by akliotzk on 6/22/2015.
 */
public class MySharedPreferences {

    private static final String PREF_NAME = "com.efedeniz.phoneoff";

    private static final String NOTIFICATION_MINUTES = "notificationMinutes";
    private static final String NOTIFICATION_RANGE = "notificationRange";
    private static final String APP_ENABLED = "appEnabled";
    private static final String IS_PAID = "isPaid";
    private static final String NUM_VIEWS = "numViews";
    private static final String RATE_PREFERENCE = "ratePreference";
    private static final String REMINDER_PREFERENCE = "reminderPreference";

    // notification minutes
    public static void putNotificationMinutes(Context context, int minutes) {
        get(context).edit().putInt(NOTIFICATION_MINUTES, minutes).commit();
    }

    public static int getNotificationMinutes(Context context) {
        return get(context).getInt(NOTIFICATION_MINUTES, 1);
    }

    // total notification range
    public static void putNotificationRange(Context context, int minutes) {
        get(context).edit().putInt(NOTIFICATION_RANGE, minutes).commit();
    }

    public static int getNotificationRange(Context context) {
        return get(context).getInt(NOTIFICATION_RANGE, 1);
    }

    // appEnabled
    public static void putAppEnabled(Context context, boolean isEnabled) {
        get(context).edit().putBoolean(APP_ENABLED, isEnabled).commit();
    }

    public static boolean getAppEnabled(Context context) {
        return get(context).getBoolean(APP_ENABLED, true);
    }

    // isPaid
    public static void putIsPaid(Context context, boolean isEnabled) {
        get(context).edit().putBoolean(IS_PAID, isEnabled).commit();
    }

    public static boolean getIsPaid(Context context) {
        return get(context).getBoolean(IS_PAID, false);
    }

    // numViews
    public static void putNumViews(Context context, int minutes) {
        get(context).edit().putInt(NUM_VIEWS, minutes).commit();
    }

    public static int getNumViews(Context context) {
        return get(context).getInt(NUM_VIEWS, 0);
    }

    // ratePreference
    public static void putRatePreference(Context context, int pref) {
        get(context).edit().putInt(RATE_PREFERENCE, pref).commit();
    }

    public static int getRatePreference(Context context) {
        return get(context).getInt(RATE_PREFERENCE, 0);
    }

    // 0 - pop up
    // 1 - notification
    public static void putReminderPreference(Context context, int pref) {
        get(context).edit().putInt(REMINDER_PREFERENCE, pref).commit();
    }

    public static int getReminderPreference(Context context) {
        return get(context).getInt(REMINDER_PREFERENCE, 0);
    }

    private static SharedPreferences get(Context context) {
        return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }
}
