package efedeniz.com.phoneoff;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.appbrain.AdService;
import com.appbrain.AppBrain;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.formats.NativeAd;

import java.util.ArrayList;
import java.util.List;

import efedeniz.com.util.IabHelper;
import efedeniz.com.util.IabResult;
import efedeniz.com.util.Inventory;
import efedeniz.com.util.Purchase;

public class PhoneOffActivity extends Activity {

    // TODO: To make a pop up use DialogActivity with MULITPLE_DOCUMENTS as a flag in Intent?
    private static final String TAG = PhoneOffActivity.class.getSimpleName();

    private static final String HIDE_ADS_ID = "hide_ads";

    private IabHelper mHelper;
    private boolean m_bIsUndoPurchase = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_off);

        // let's init AppBrain
        AppBrain.init(this);

        ImageButton moreAppsButton = (ImageButton)findViewById(R.id.more_apps_button);
        AdService ads = AppBrain.getAds();
        ads.setOfferWallClickListener(getApplicationContext(), moreAppsButton);

        // let's load google play services
        mHelper = new IabHelper(this, getResources().getString(R.string.base64_public_key));

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    Log.d(TAG, "Problem setting up In-app Billing: " + result);
                }
                else {
                    // Hooray, IAB is fully set up!
                    Log.d(TAG, "IAB is set up... let's check the in-app purchases");
                    List additionalSkuList = new ArrayList<String>();
                    additionalSkuList.add(HIDE_ADS_ID);
                    mHelper.queryInventoryAsync(true, additionalSkuList, mQueryFinishedListener);
                }
            }
        });

        // show ads
        if (!MySharedPreferences.getIsPaid(getApplicationContext())) {
            Log.d(TAG, "Show ad");
            AdView mAdView = (AdView) findViewById(R.id.adView);
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
        }
        else {
            moreAppsButton = (ImageButton)findViewById(R.id.more_apps_button);
            moreAppsButton.setVisibility(View.INVISIBLE);
        }

        Logger.clearLog(getApplicationContext());

        // let's start service if already not started
        Log.d(TAG, "start ScreenMonitorService");
        startService(new Intent(this, ScreenMonitorService.class));


        // set the default progress and minute text view
        final SeekBar minuteBar = (SeekBar)findViewById(R.id.minutesSlider);
        minuteBar.setProgress(MySharedPreferences.getNotificationMinutes(getApplicationContext()) - 1);

        final SeekBar rangeBar = (SeekBar)findViewById(R.id.totalMinutesSlider);
        rangeBar.setProgress(MySharedPreferences.getNotificationRange(getApplicationContext()) - 1);

        setMinuteTextView(minuteBar.getProgress(), rangeBar.getProgress());

        // set the radio buttons
        if (MySharedPreferences.getAppEnabled(getApplicationContext())) {
            minuteBar.setEnabled(true);
            rangeBar.setEnabled(true);
            TextView minuteView = (TextView)findViewById(R.id.minutesTextView);
            minuteView.setTextColor(Color.BLACK);
            TextView notifyMeText = (TextView)findViewById(R.id.notify_me_text);
            notifyMeText.setTextColor(Color.BLACK);

            RadioButton enableButton = (RadioButton)findViewById(R.id.enable_button);
            enableButton.setChecked(true);

            RadioButton popUpRadioButton = (RadioButton)findViewById(R.id.pop_up_button);
            popUpRadioButton.setEnabled(true);
            popUpRadioButton.setTextColor(Color.BLACK);

            RadioButton notificationButton = (RadioButton)findViewById(R.id.notification_button);
            notificationButton.setEnabled(true);
            notificationButton.setTextColor(Color.BLACK);

            TextView remindMeText = (TextView)findViewById(R.id.remind_me_text);
            remindMeText.setTextColor(Color.BLACK);

            NotificationController.scheduleNotification(getApplicationContext());
        }
        else {
            minuteBar.setEnabled(false);
            rangeBar.setEnabled(false);
            TextView minuteView = (TextView)findViewById(R.id.minutesTextView);
            minuteView.setTextColor(Color.GRAY);
            TextView notifyMeText = (TextView)findViewById(R.id.notify_me_text);
            notifyMeText.setTextColor(Color.GRAY);

            RadioButton disableButton = (RadioButton)findViewById(R.id.disable_button);
            disableButton.setChecked(true);

            RadioButton popUpRadioButton = (RadioButton)findViewById(R.id.pop_up_button);
            popUpRadioButton.setEnabled(false);
            popUpRadioButton.setTextColor(Color.GRAY);

            RadioButton notificationButton = (RadioButton)findViewById(R.id.notification_button);
            notificationButton.setEnabled(false);
            notificationButton.setTextColor(Color.GRAY);

            TextView remindMeText = (TextView)findViewById(R.id.remind_me_text);
            remindMeText.setTextColor(Color.GRAY);
        }

        minuteBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                setMinuteParams(seekBar, rangeBar);
                if (seekBar.getProgress() > rangeBar.getProgress()) {
                    rangeBar.setProgress(progress);
                    setRangeParams(seekBar, rangeBar);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        rangeBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                setRangeParams(minuteBar, seekBar);
                if (seekBar.getProgress() < minuteBar.getProgress()) {
                    minuteBar.setProgress(progress);
                    setMinuteParams(minuteBar, seekBar);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        if (MySharedPreferences.getReminderPreference(getApplicationContext()) == 0) {
            RadioButton popUpButton = (RadioButton)findViewById(R.id.pop_up_button);
            popUpButton.setChecked(true);
        }
        else if (MySharedPreferences.getReminderPreference(getApplicationContext()) == 1) {
            RadioButton notificationButton = (RadioButton)findViewById(R.id.notification_button);
            notificationButton.setChecked(true);
        }

        int numViews = MySharedPreferences.getNumViews(getApplicationContext());
        if (numViews == 10 || numViews == 20 || numViews == 50) {

            // let's have a pop up here that notifies lets the user know
            // about their phone usage limits
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(PhoneOffActivity.this);
            alertBuilder.setMessage("Hopefully using PhoneOff has led to you using your smartphone less.\n\nKeep " +
                    "improving yourself and try lowering the phone usage limit.\n\nYou can do it!");
            alertBuilder.setPositiveButton("Got it",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alertDialog = alertBuilder.create();
            alertDialog.show();

            MySharedPreferences.putNumViews(getApplicationContext(), numViews + 1);
        }

        NotificationController.appLaunched(getApplicationContext());
    }

    private void setMinuteParams(SeekBar minuteBar, SeekBar rangeBar) {
        setMinuteTextView(minuteBar.getProgress(), rangeBar.getProgress());
        MySharedPreferences.putNotificationMinutes(getApplicationContext(), minuteBar.getProgress() + 1);
        NotificationController.scheduleNotification(getApplicationContext());
    }

    private void setRangeParams(SeekBar minuteBar, SeekBar rangeBar) {
        setMinuteTextView(minuteBar.getProgress(), rangeBar.getProgress());
        MySharedPreferences.putNotificationRange(getApplicationContext(), rangeBar.getProgress() + 1);
        NotificationController.scheduleNotification(getApplicationContext());
    }

    public void onReminderRadioButtonClikced (View view) {
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.pop_up_button:
                MySharedPreferences.putReminderPreference(getApplicationContext(), 0);
                break;
            case R.id.notification_button:
                MySharedPreferences.putReminderPreference(getApplicationContext(), 1);
                break;
        }
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.enable_button:
                if (checked) {
                    MySharedPreferences.putAppEnabled(getApplicationContext(), true);
                    NotificationController.scheduleNotification(getApplicationContext());
                    final SeekBar minuteBar = (SeekBar) findViewById(R.id.minutesSlider);
                    minuteBar.setEnabled(true);
                    final SeekBar rangeBar = (SeekBar)findViewById(R.id.totalMinutesSlider);
                    rangeBar.setEnabled(true);
                    Log.d(TAG, "Enable app");
                    TextView minuteView = (TextView)findViewById(R.id.minutesTextView);
                    minuteView.setTextColor(Color.BLACK);
                    TextView notifyMeText = (TextView)findViewById(R.id.notify_me_text);
                    notifyMeText.setTextColor(Color.BLACK);

                    RadioButton popUpRadioButton = (RadioButton)findViewById(R.id.pop_up_button);
                    popUpRadioButton.setEnabled(true);
                    popUpRadioButton.setTextColor(Color.BLACK);

                    RadioButton notificationButton = (RadioButton)findViewById(R.id.notification_button);
                    notificationButton.setEnabled(true);
                    notificationButton.setTextColor(Color.BLACK);

                    TextView remindMeText = (TextView)findViewById(R.id.remind_me_text);
                    remindMeText.setTextColor(Color.BLACK);
                }
                break;
            case R.id.disable_button:
                if (checked) {
                    MySharedPreferences.putAppEnabled(getApplicationContext(), false);
                    NotificationController.cancelNotification(getApplicationContext());
                    final SeekBar minuteBar = (SeekBar) findViewById(R.id.minutesSlider);
                    minuteBar.setEnabled(false);
                    final SeekBar rangeBar = (SeekBar)findViewById(R.id.totalMinutesSlider);
                    rangeBar.setEnabled(false);
                    Log.d(TAG, "Disable app");
                    TextView minuteView = (TextView)findViewById(R.id.minutesTextView);
                    minuteView.setTextColor(Color.GRAY);
                    TextView notifyMeText = (TextView)findViewById(R.id.notify_me_text);
                    notifyMeText.setTextColor(Color.GRAY);

                    RadioButton popUpRadioButton = (RadioButton)findViewById(R.id.pop_up_button);
                    popUpRadioButton.setEnabled(false);
                    popUpRadioButton.setTextColor(Color.GRAY);

                    RadioButton notificationButton = (RadioButton)findViewById(R.id.notification_button);
                    notificationButton.setEnabled(false);
                    notificationButton.setTextColor(Color.GRAY);

                    TextView remindMeText = (TextView)findViewById(R.id.remind_me_text);
                    remindMeText.setTextColor(Color.GRAY);
                }
                break;
        }
    }


    private void setMinuteTextView(int minuteBarProgress, int rangeBarProgress) {
        TextView minuteView = (TextView)findViewById(R.id.minutesTextView);

        if (rangeBarProgress == minuteBarProgress) {
            minuteView.setText( (minuteBarProgress + 1) + " minutes in a row" );
        }

        else {
            if (minuteBarProgress == 0) {
                minuteView.setText((minuteBarProgress + 1) + " out of " + (rangeBarProgress + 1) + " minutes ");
            }
            else {
                minuteView.setText((minuteBarProgress + 1) + " out of " + (rangeBarProgress + 1) + " minutes ");
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_phone_off, menu);
        // add an option for AppBrain offer wall
        AdService ads = AppBrain.getAds();
        MenuItem item = menu.add(ads.getOfferWallButtonLabel(this));
        ads.setOfferWallMenuItemClickListener(this, item);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_hide_ads) {
            if (MySharedPreferences.getIsPaid(getApplicationContext()))  {
                // show thank you dialog
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(PhoneOffActivity.this);
                alertBuilder.setMessage("You have already purchased the hide ads upgrade and have earned some serious karma. Thank you!!");
                alertBuilder.setPositiveButton("I'm Awesome",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alertDialog = alertBuilder.create();
                alertDialog.show();
            }
            else {
                // launch the purchase ad flow
                mHelper.launchPurchaseFlow(this, HIDE_ADS_ID, 10001,
                        mPurchaseFinishedListener, "");
                Log.d(TAG, "Launch purchase flow");
            }
            return true;
        }

        // TODO: Remove this... just for testing
        /*
        else if (id == R.id.action_undo_purchase) {
            // let's query the inventory to and consume the purchase if exists
            m_bIsUndoPurchase = true;
            List additionalSkuList = new ArrayList<String>();
            additionalSkuList.add(HIDE_ADS_ID);
            mHelper.queryInventoryAsync(true, additionalSkuList, mQueryFinishedListener);
        }
        */

        else if (id == R.id.action_rate_and_review) {
            final Uri uri = Uri.parse("market://details?id=" + getApplicationContext().getPackageName());
            final Intent rateAppIntent = new Intent(Intent.ACTION_VIEW, uri);

            if (getPackageManager().queryIntentActivities(rateAppIntent, 0).size() > 0)
            {
                startActivity(rateAppIntent);
            }
            else
            {
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(PhoneOffActivity.this);
                alertBuilder.setMessage("Your device cannot reach the Android Market. Please ensure that Google Play app is installed.");
                alertBuilder.setPositiveButton("Got it",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alertDialog = alertBuilder.create();
                alertDialog.show();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
            = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase)
        {
            if (result.isFailure()) {
                Log.d(TAG, "Error purchasing: " + result);
                return;
            }
            else if (purchase.getSku().equals(HIDE_ADS_ID)) {
                Log.d(TAG, "Hide ads purchased!");
                MySharedPreferences.putIsPaid(getApplicationContext(), true);
                AdView mAdView = (AdView) findViewById(R.id.adView);
                mAdView.setVisibility(View.INVISIBLE);

                Log.d(TAG, "purchased this item - " + purchase.getSku());

                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(PhoneOffActivity.this);
                alertBuilder.setMessage("Thank you for purchasing the Hide Ads upgrade. You have chosen to spread the positivity and pay it forward. Karma is coming your way!");
                alertBuilder.setPositiveButton("I'm Awesome",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alertDialog = alertBuilder.create();
                alertDialog.show();

                ImageButton moreAppsButton = (ImageButton)findViewById(R.id.more_apps_button);
                moreAppsButton.setVisibility(View.INVISIBLE);
            }
        }
    };

    IabHelper.QueryInventoryFinishedListener
            mQueryFinishedListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory)
        {
            if (result.isFailure()) {
                Log.d(TAG, "Error from querying IAB");
                return;
            }

            if (inventory != null && inventory.hasDetails(HIDE_ADS_ID)) {

                String hideAdPrice =
                        inventory.getSkuDetails(HIDE_ADS_ID).getPrice();

                Log.d(TAG, "hideAdPrice is " + hideAdPrice);

                if (!inventory.hasPurchase(HIDE_ADS_ID)) {

                }
                else if (m_bIsUndoPurchase) {
                    // consume item
                    mHelper.consumeAsync(inventory.getPurchase(HIDE_ADS_ID),
                            mConsumeFinishedListener);
                }
            }
            else {
                Log.d(TAG, "No information available for HIDE_ADS_ID");
            }

            // update the UI
        }
    };


    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
            new IabHelper.OnConsumeFinishedListener() {
                public void onConsumeFinished(Purchase purchase, IabResult result) {
                    if (result.isSuccess()) {
                        Log.d(TAG, "Consume purchase");
                        MySharedPreferences.putIsPaid(getApplicationContext(), false);
                        AdView mAdView = (AdView) findViewById(R.id.adView);
                        mAdView.setVisibility(View.VISIBLE);
                        AdRequest adRequest = new AdRequest.Builder().build();
                        mAdView.loadAd(adRequest);
                    }
                    else {
                        Log.d(TAG, "Error consuming purchase");
                        // handle error
                    }
                }
            };

    // Need this for IAB for some reason...
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        }
        else {
            Log.d(TAG, "onActivityResult handled by IABUtil.");
        }
    }
}
