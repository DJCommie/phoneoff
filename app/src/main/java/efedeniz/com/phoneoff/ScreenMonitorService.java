package efedeniz.com.phoneoff;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by akliotzk on 7/15/2015.
 */
public class ScreenMonitorService extends Service {

    final private static String TAG = ScreenMonitorService.class.getSimpleName();
    BroadcastReceiver screenEventReceiver = null;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d(TAG, "onStartCommand");

        // let's register our receivers and do nothing more
        final IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_SCREEN_ON);

        screenEventReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();

                if (action.equals(Intent.ACTION_SCREEN_OFF)) {
                    NotificationController.onScreenOff(getApplicationContext());
                } else if (action.equals(Intent.ACTION_SCREEN_ON)) {
                    NotificationController.onScreenOn(getApplicationContext());
                }
            }
        };
        registerReceiver(screenEventReceiver, filter);

        return START_STICKY;
    }


    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        unregisterReceiver(screenEventReceiver);

        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
